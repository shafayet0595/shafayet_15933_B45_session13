<?php
//__construct;__destruct;
class person{
    public $name="default name";
    public $address="default address";
    public $phone="default phone";

    public function __construct()
    {
        echo "I'm inside ".__METHOD__." method<br>";
    }
    public function __destruct()
    {
        echo "I'm inside ".__METHOD__." method<br>";
    }

    public function doSomething()
    {
        echo "Doing something<br>";
    }
    //static method(N:B: property can be staic too)
    public static $static = "Im static property";
    public static function doFrequently(){
        echo "Im doing it frequently<br>";
    }


    public function __set($name, $value)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong property = $name<br>";
        echo "Wrong Property value = $value";
    }

    public function __get($name)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong property = $name";
    }

    public function __isset($name)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong property = $name";
    }

    public function __sleep()
    {
        return array("name","phone");
    }
    public function __wakeup()
    {
        echo "i m waking it up";
    }

    public function __unset($name)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong property = $name";
    }

    public function __call($name, $arguments)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong method = $name";
        echo "<pre>";
         print_r($arguments);
        echo "</pre>";
    }
    public static function __callStatic($name, $arguments)
    {
        echo "I'm inside ".__METHOD__." method<br>";
        echo "Wrong static method = $name";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public function __toString()
    {
        return 'i m an object $newObj'."<br>";
    }
    public function __invoke()
    {
        // TODO: Implement __invoke() method.
    }
}

class dummy{
    public $dummy;
    public function __construct()
    {
        echo "I'm inside ".__METHOD__." method<br>";
    }

}
$obj = new person();
person::doFrequently();
person::doFrequentlyyy(1111,"hey");
echo person::$static."<br>";

$obj->DOB ="Setting this string";
if(isset($obj->asdads)){

}
unset($obj->sdasdasdd);

echo "hello there<br>";
$objAnother = new dummy();
$obj->doSomething1("value");
//unset($obj);


$myvar = serialize($obj);
echo"<pre>";
var_dump($myvar);
echo"</pre>";
$newObj = unserialize($myvar);
echo"<pre>";
var_dump($newObj);
echo"</pre>";
echo $newObj;